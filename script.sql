USE [master]
GO
/****** Object:  Database [ParkingAppDatabase]    Script Date: 6/6/2022 12:28:36 AM ******/
CREATE DATABASE [ParkingAppDatabase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ParkingAppDatabase', FILENAME = N'G:\sqk\MSSQL15.MSSQLSERVER02\MSSQL\DATA\ParkingAppDatabase.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ParkingAppDatabase_log', FILENAME = N'G:\sqk\MSSQL15.MSSQLSERVER02\MSSQL\DATA\ParkingAppDatabase_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ParkingAppDatabase] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ParkingAppDatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ParkingAppDatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ParkingAppDatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ParkingAppDatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ParkingAppDatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ParkingAppDatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET RECOVERY FULL 
GO
ALTER DATABASE [ParkingAppDatabase] SET  MULTI_USER 
GO
ALTER DATABASE [ParkingAppDatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ParkingAppDatabase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ParkingAppDatabase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ParkingAppDatabase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ParkingAppDatabase] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ParkingAppDatabase] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'ParkingAppDatabase', N'ON'
GO
ALTER DATABASE [ParkingAppDatabase] SET QUERY_STORE = OFF
GO
USE [ParkingAppDatabase]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [int] NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCard]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](50) NOT NULL,
	[ActionDate] [nvarchar](50) NOT NULL,
	[CVV] [nvarchar](3) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_CreditCard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Parking]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](150) NOT NULL,
	[HorizontalSize] [int] NOT NULL,
	[VerticalSize] [int] NOT NULL,
 CONSTRAINT [PK_Parking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ParkingPlace]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParkingPlace](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParkingId] [int] NOT NULL,
	[PalaceId] [int] NOT NULL,
 CONSTRAINT [PK_ParkingPlace] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ParkingSecurity]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParkingSecurity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ParkingId] [int] NOT NULL,
 CONSTRAINT [PK_ParkingSecurity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [float] NOT NULL,
	[PlaceId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Place]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Place](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NULL,
	[UserId] [int] NULL,
	[X] [int] NOT NULL,
	[Y] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CostPerDay] [float] NOT NULL,
	[PaidUpTo] [datetime] NULL,
 CONSTRAINT [PK_Place] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Color] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 6/6/2022 12:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[TelegramChatId] [nvarchar](100) NULL,
	[Balance] [float] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Contact] ([Id], [PhoneNumber], [Email]) VALUES (1, N'+7 (646) 335 324 222', N'ggsdgsdg@mail.rfff')
GO
SET IDENTITY_INSERT [dbo].[CreditCard] ON 

INSERT [dbo].[CreditCard] ([Id], [Number], [ActionDate], [CVV], [UserId]) VALUES (1, N'1111222233334444', N'11/11', N'222', 1)
INSERT [dbo].[CreditCard] ([Id], [Number], [ActionDate], [CVV], [UserId]) VALUES (2, N'1111222233334444', N'11/12', N'221', 1)
INSERT [dbo].[CreditCard] ([Id], [Number], [ActionDate], [CVV], [UserId]) VALUES (3, N'1111366686688946', N'12/13', N'234', 1)
INSERT [dbo].[CreditCard] ([Id], [Number], [ActionDate], [CVV], [UserId]) VALUES (4, N'5234523523532222', N'11/33', N'888', 1)
INSERT [dbo].[CreditCard] ([Id], [Number], [ActionDate], [CVV], [UserId]) VALUES (1004, N'2532454334533453', N'11/11', N'222', 2)
SET IDENTITY_INSERT [dbo].[CreditCard] OFF
GO
SET IDENTITY_INSERT [dbo].[Parking] ON 

INSERT [dbo].[Parking] ([Id], [Name], [Address], [HorizontalSize], [VerticalSize]) VALUES (1, N'TSUM', N'Moscow', 600, 500)
INSERT [dbo].[Parking] ([Id], [Name], [Address], [HorizontalSize], [VerticalSize]) VALUES (3, N'ssdgsadg', N'asdfasdgsadfsdsggs', 551, 500)
INSERT [dbo].[Parking] ([Id], [Name], [Address], [HorizontalSize], [VerticalSize]) VALUES (4, N'hfhdfhdfg', N'dfhdfgsdfg', 500, 550)
SET IDENTITY_INSERT [dbo].[Parking] OFF
GO
SET IDENTITY_INSERT [dbo].[ParkingPlace] ON 

INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (80, 3, 1)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (81, 3, 2)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (82, 3, 3)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (83, 3, 4)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (84, 1, 5)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (85, 1, 6)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (86, 1, 7)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (87, 1, 8)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (88, 1, 9)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (89, 1, 10)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (90, 4, 11)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (91, 4, 12)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (92, 4, 13)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (93, 4, 14)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (94, 4, 15)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (95, 1, 16)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (96, 1, 17)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (97, 1, 18)
INSERT [dbo].[ParkingPlace] ([Id], [ParkingId], [PalaceId]) VALUES (98, 1, 19)
SET IDENTITY_INSERT [dbo].[ParkingPlace] OFF
GO
SET IDENTITY_INSERT [dbo].[ParkingSecurity] ON 

INSERT [dbo].[ParkingSecurity] ([Id], [UserId], [ParkingId]) VALUES (6, 3, 3)
SET IDENTITY_INSERT [dbo].[ParkingSecurity] OFF
GO
SET IDENTITY_INSERT [dbo].[Payment] ON 

INSERT [dbo].[Payment] ([Id], [Value], [PlaceId], [CreatedDate]) VALUES (15, 10, 7, CAST(N'2022-05-25T11:28:21.227' AS DateTime))
INSERT [dbo].[Payment] ([Id], [Value], [PlaceId], [CreatedDate]) VALUES (16, 10, 10, CAST(N'2022-05-25T11:30:15.073' AS DateTime))
INSERT [dbo].[Payment] ([Id], [Value], [PlaceId], [CreatedDate]) VALUES (1019, 10, 16, CAST(N'2022-06-05T23:52:52.500' AS DateTime))
INSERT [dbo].[Payment] ([Id], [Value], [PlaceId], [CreatedDate]) VALUES (1020, 10, 5, CAST(N'2022-06-05T23:54:22.657' AS DateTime))
INSERT [dbo].[Payment] ([Id], [Value], [PlaceId], [CreatedDate]) VALUES (1021, 10, 4, CAST(N'2022-06-05T23:55:10.610' AS DateTime))
INSERT [dbo].[Payment] ([Id], [Value], [PlaceId], [CreatedDate]) VALUES (1022, 20, 1015, CAST(N'2022-06-05T23:55:22.907' AS DateTime))
SET IDENTITY_INSERT [dbo].[Payment] OFF
GO
SET IDENTITY_INSERT [dbo].[Place] ON 

INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (1, 55, NULL, 327, 175, 3, 12, CAST(N'2022-06-07T03:56:13.913' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (2, 222, NULL, 78, 222, 3, 10, CAST(N'2022-06-07T06:06:22.340' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (3, 5, NULL, 247, 316, 3, 10, CAST(N'2022-06-07T05:23:09.050' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (4, 4, 2, 254, 122, 1, 10, CAST(N'2022-06-06T23:55:10.583' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (5, 1, 2, 184, 89, 1, 10, CAST(N'2022-06-06T23:54:22.607' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (6, 4, 1, 488, 224, 1, 10, CAST(N'2022-06-07T03:45:40.263' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (7, 2, 1, 110, 197, 1, 10, CAST(N'2022-06-07T11:28:21.143' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (8, 3, 1, 358, 105, 1, 10, CAST(N'2022-06-07T03:50:10.500' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (9, 12, 1, 140, 402, 1, 10, CAST(N'2022-06-07T04:07:55.277' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (10, 444, 1, 274, 395, 1, 10, CAST(N'2022-06-07T11:30:15.063' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (11, NULL, NULL, 366, 172, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (12, NULL, NULL, 192, 463, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (13, NULL, NULL, 228, 254, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (14, NULL, NULL, 390, 413, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (15, NULL, NULL, 132, 254, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (16, 33, NULL, 236, 275, 3, 10, CAST(N'2022-06-06T23:52:52.443' AS DateTime))
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (17, NULL, NULL, 359, 254, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (18, NULL, NULL, 476, 94, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (19, NULL, NULL, 55, 288, 3, 10, NULL)
INSERT [dbo].[Place] ([Id], [Number], [UserId], [X], [Y], [StatusId], [CostPerDay], [PaidUpTo]) VALUES (1015, 1, 2, 184, 89, 8, 10, CAST(N'2022-06-06T23:54:22.607' AS DateTime))
SET IDENTITY_INSERT [dbo].[Place] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([Id], [Name]) VALUES (1, N'Security')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (2, N'Client')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (1, N'Occupied', N'Red')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (2, N'Booked', N'Yellow')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (3, N'Freely', N'Green')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (4, N'Occupied', N'Red')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (5, N'Occupied', N'Red')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (6, N'Occupied', N'Red')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (7, N'Occupied', N'Red')
INSERT [dbo].[Status] ([Id], [Name], [Color]) VALUES (8, N'Occupied', N'Red')
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [RoleId], [TelegramChatId], [Balance]) VALUES (1, N'Alexander Staff', N'1', N'1', 1, NULL, 74)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [RoleId], [TelegramChatId], [Balance]) VALUES (2, N'Ivan Client', N'2', N'2', 2, N'911331089', 50)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [RoleId], [TelegramChatId], [Balance]) VALUES (3, N'Igor Egdgd', N'3', N'3', 1, N'841862151', 0)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [RoleId], [TelegramChatId], [Balance]) VALUES (4, N'Timur Dgsd', N'4', N'4', 1, N'841862151', 0)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [RoleId], [TelegramChatId], [Balance]) VALUES (5, N'Andrey LKgds', N'5', N'5', 1, N'841862151', 0)
INSERT [dbo].[User] ([Id], [FullName], [Login], [Password], [RoleId], [TelegramChatId], [Balance]) VALUES (6, N'Aboba', N'Aboba', N'Aboba', 2, NULL, 0)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[CreditCard]  WITH CHECK ADD  CONSTRAINT [FK_CreditCard_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CreditCard] CHECK CONSTRAINT [FK_CreditCard_User]
GO
ALTER TABLE [dbo].[ParkingPlace]  WITH CHECK ADD  CONSTRAINT [FK_ParkingPlace_Parking] FOREIGN KEY([ParkingId])
REFERENCES [dbo].[Parking] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ParkingPlace] CHECK CONSTRAINT [FK_ParkingPlace_Parking]
GO
ALTER TABLE [dbo].[ParkingPlace]  WITH CHECK ADD  CONSTRAINT [FK_ParkingPlace_Place] FOREIGN KEY([PalaceId])
REFERENCES [dbo].[Place] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ParkingPlace] CHECK CONSTRAINT [FK_ParkingPlace_Place]
GO
ALTER TABLE [dbo].[ParkingSecurity]  WITH CHECK ADD  CONSTRAINT [FK_ParkingSecurity_Parking] FOREIGN KEY([ParkingId])
REFERENCES [dbo].[Parking] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ParkingSecurity] CHECK CONSTRAINT [FK_ParkingSecurity_Parking]
GO
ALTER TABLE [dbo].[ParkingSecurity]  WITH CHECK ADD  CONSTRAINT [FK_ParkingSecurity_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[ParkingSecurity] CHECK CONSTRAINT [FK_ParkingSecurity_User]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_Place] FOREIGN KEY([PlaceId])
REFERENCES [dbo].[Place] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_Place]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK_Place_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK_Place_Status]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK_Place_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK_Place_User]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
USE [master]
GO
ALTER DATABASE [ParkingAppDatabase] SET  READ_WRITE 
GO

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingAppWebAPI.Controllers
{
    public class ContactController : ApiController
    {
        [Route("api/GetContact")]
        public Models.Contact Get()
        {
            Models.Contact contact = HomeController.db.Contact.FirstOrDefault();
            return contact;
        }
    }
}

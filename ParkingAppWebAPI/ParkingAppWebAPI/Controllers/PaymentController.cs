﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingAppWebAPI.Controllers
{
    public class PaymentController : ApiController
    {
        // GET: api/Payment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Payment/5
        [Route("api/GetPaymentConcreteUser")]
        public ConceretUserPaymentList GetPaymentConcreteUser(int id)
        {
            ConceretUserPaymentList conceretUserPaymentList = new ConceretUserPaymentList();
            conceretUserPaymentList.payments = HomeController.db.Payment.Where(c => c.Place.UserId == id).ToList();
            return conceretUserPaymentList;
        }
        public class ConceretUserPaymentList
        {
            public List<Models.Payment> payments { get; set; }
        }
        // POST: api/Payment
        [Route("api/AddBalance")]
        [HttpPost]
        public double AddBalance(HttpRequestMessage request)
        {
            UserSum paymentParkingPlace = JsonConvert.DeserializeObject<UserSum>(request.Content.ReadAsStringAsync().Result);
            Models.User user = HomeController.db.User.FirstOrDefault(c => c.Id == paymentParkingPlace.User.Id);
            user.Balance += paymentParkingPlace.Sum;
            HomeController.db.SaveChanges();
            try
            {
                HomeController.client.SendTextMessageAsync(user.TelegramChatId, $"Ваш баланс был пополнен на: {paymentParkingPlace.Sum}");
            }
            catch
            {

            }
            return user.Balance;
        }
        [Route("api/ExtendRent")]
        [HttpPost]
        public double ExtendRent(HttpRequestMessage request)
        {
            PlaceSum placeSum = JsonConvert.DeserializeObject<PlaceSum>(request.Content.ReadAsStringAsync().Result);
            Models.User user = HomeController.db.User.FirstOrDefault(c=>c.Id == placeSum.parkingPlace.Place.UserId);
            user.Balance -= placeSum.Sum;
            Models.Payment payment = new Models.Payment();
            payment.Place = placeSum.parkingPlace.Place;
            payment.Value = placeSum.Sum;
            payment.CreatedDate = DateTime.Now;
            HomeController.db.Payment.Add(payment);
            Models.ParkingPlace parkingPlace = HomeController.db.ParkingPlace.FirstOrDefault(c => c.Id == placeSum.parkingPlace.Id);
            Models.Place place = HomeController.db.Place.FirstOrDefault(c => c.Id == parkingPlace.Place.Id);
            DateTime? dateTime = new DateTime?();
            dateTime = place.PaidUpTo;
            place.PaidUpTo = dateTime.Value.AddDays(placeSum.Sum / parkingPlace.Place.CostPerDay);
            HomeController.db.SaveChanges();
            try
            {
                HomeController.client.SendTextMessageAsync(user.TelegramChatId, $"С вашего баланса было списано: {placeSum.Sum}\nПричина: Продление аренды на место {placeSum.parkingPlace.Place.Number}");
            }
            catch
            {

            }
            return user.Balance;
        }
        class PlaceSum
        {
            public double Sum { get; set; }
            public Models.ParkingPlace parkingPlace { get; set; }
        }
        class UserSum
        {
            public double Sum { get; set; }
            public ParkingAppWebAPI.Models.User User { get; set; }
        }
        // PUT: api/Payment/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Payment/5
        public void Delete(int id)
        {
        }
    }
}

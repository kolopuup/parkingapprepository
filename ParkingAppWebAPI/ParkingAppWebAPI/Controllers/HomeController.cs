﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Web.Mvc;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace ParkingAppWebAPI.Controllers
{
    public class HomeController : Controller
    {
        public static Models.ParkingAppDatabaseEntities db = new Models.ParkingAppDatabaseEntities();
        private const string token = "5160811509:AAHBXxW2mz0LQVlswlUQD22fVBXu7R3yobU";
        public static TelegramBotClient client;
        public ActionResult Index()
        {
            client = new TelegramBotClient(token);
            client.OnMessage += Client_OnMessage;
            Task.Run(() => StartReceiving());
            ViewBag.Title = "Home Page";
            Timer timer = new Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
            return View();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            List<Models.Place> parkingPlaces = new List<Models.Place>();
            parkingPlaces = db.Place.Where(c => c.PaidUpTo < DateTime.Now && c.UserId != null).ToList();
            if (parkingPlaces.Count == 0)
                return;
            foreach(var c in parkingPlaces)
            {
                if (c.User.TelegramChatId is null)
                    return;
                else
                    client.SendTextMessageAsync(c.User.TelegramChatId, $"Вы больше не арендуете парковочное место в центре {c.Number}\nПо причине неуплаты аренды");
                c.UserId = null;
                c.StatusId = 3;
            }
            
            HomeController.db.SaveChanges();
        }

        private async Task StartReceiving()
        {
            client.StartReceiving();
        }

        private async void Client_OnMessage(object sender, MessageEventArgs e)
        {
            string MessageChatId = Convert.ToString(e.Message.Chat.Id);
            Models.User user = db.User.FirstOrDefault(c => c.TelegramChatId == MessageChatId);
            if (db.User.FirstOrDefault(c => c.TelegramChatId == MessageChatId) != null)
            {
                if(e.Message.Text == "/exit")
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, $"Вы успешно вышли из аккаунта");
                    user.TelegramChatId = null;
                    HomeController.db.SaveChanges();
                }
                if(e.Message.Text == "/balance")
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, $"Ваш баланс: {user.Balance}");
                }
                if(e.Message.Text == "/allmyplaces")
                {
                    List<Models.ParkingPlace> parkingPlaces = db.ParkingPlace.Where(c=>c.Place.UserId == user.Id).ToList();
                    if(parkingPlaces.Count == 0)
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, $"У вас нет парковочных мест");
                    }
                    foreach(var c in parkingPlaces)
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, $"Парковка: {c.Parking.Name}\nНомер: {c.Place.Number}\nСтоимость: {c.Place.CostPerDay}\nОплачено до: {c.Place.PaidUpTo}");
                    }
                    return;
                }
            }
            if (db.User.FirstOrDefault(c => c.TelegramChatId == MessageChatId) == null)
            {

                if (e.Message.Text == "/start" && e.Message.Text != null && db.User.FirstOrDefault(c => c.TelegramChatId == MessageChatId) is null)
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Для использования бота вам необходимо авторизироваться\nВведите логин пароль в формате:\nlogin:password\nИли зарегестрируйтесь с помощью команды /registration");
                    return;
                }

                if (e.Message.Text.Contains("/registration") && e.Message.Text != null)
                {
                    client.SendTextMessageAsync(e.Message.Chat.Id, "Введите данные через пробел:\nFullName Login Password");
                    return;
                }

                if (e.Message.Text.Contains(" "))
                {
                    string FullName;
                    string Login;
                    string Password;
                    string[] split = e.Message.Text.Split(' ');
                    FullName = split[0];
                    Login = split[1];
                    Password = split[2];
                    Models.User newUser = new Models.User() { FullName = FullName, Login = Login, Password = Password, RoleId = 2, TelegramChatId = MessageChatId };
                    HomeController.db.User.Add(newUser);
                    HomeController.db.SaveChanges();
                    client.SendTextMessageAsync(e.Message.Chat.Id, $"Вы зерегестрировались");
                    return;

                }
                if (e.Message.Text.Contains(":"))
                {
                    string login;
                    string password;
                    string[] split = e.Message.Text.Split(':');
                    login = split[0];
                    password = split[1];
                    Models.User loggedUser = db.User.FirstOrDefault(c => c.Login == login && c.Password == password);
                    if (loggedUser is null)
                    {
                        client.SendTextMessageAsync(e.Message.Chat.Id, "Пользователь не найден");
                        return;
                    }
                    if (loggedUser != null)
                    {
                        loggedUser.TelegramChatId = MessageChatId;
                        db.SaveChanges();
                        client.SendTextMessageAsync(e.Message.Chat.Id, $"{loggedUser.FullName} поздравялем вас с авторизацией");
                    }
                    return;
                }
            }

        }
    }
}

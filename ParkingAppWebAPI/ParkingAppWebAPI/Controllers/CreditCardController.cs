﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingAppWebAPI.Controllers
{
    public class CreditCardController : ApiController
    {
        // GET: api/CreditCard
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [Route("api/GetAllCardConcreteUser")]
        public List<Models.CreditCard> GetCardConcreteUser(int Id)
        {
            return HomeController.db.CreditCard.Where(c => c.UserId == Id).ToList();
            
        }

        // POST: api/CreditCard
        [HttpPost]
        [Route("api/AddCreditCard")]
        public void Post(HttpRequestMessage request)
        {
            Models.CreditCard creditCard = JsonConvert.DeserializeObject<Models.CreditCard>(request.Content.ReadAsStringAsync().Result);
            HomeController.db.CreditCard.Add(creditCard);
            HomeController.db.SaveChanges();
        }

        // PUT: api/CreditCard/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CreditCard/5
        public void Delete(int id)
        {
        }
    }
}

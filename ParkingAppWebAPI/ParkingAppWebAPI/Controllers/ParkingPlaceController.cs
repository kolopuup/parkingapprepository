﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ParkingAppWebAPI.Controllers
{
    public class ParkingPlaceController : ApiController
    {
        // GET: api/ParkingPlace
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ParkingPlace/5
        [Route("api/GetFreelyPlaces")]
        public ListParkingPlace GetFreelyPlaces()
        {
            ListParkingPlace listParkingPlace = new ListParkingPlace();
            listParkingPlace.listParkingPlace = HomeController.db.ParkingPlace.Where(c => c.Place.StatusId == 3 && c.Place.Number != null).ToList();
            return listParkingPlace;
        }
        [Route("api/GetUserPlaces")]
        public ListParkingPlace GetUserPlaces(int Id)
        {
            ListParkingPlace listParkingPlace = new ListParkingPlace();
            listParkingPlace.listParkingPlace = HomeController.db.ParkingPlace.Where(c => c.Place.UserId == Id).ToList();
            return listParkingPlace;
        }
        [Route("api/PostRentUserPlace")]
        [HttpPost]
        public void PostRentUserPlace(HttpRequestMessage request)
        {
            Models.ParkingPlace parkingPlace = JsonConvert.DeserializeObject<Models.ParkingPlace>(request.Content.ReadAsStringAsync().Result);
            Models.ParkingPlace postParkingPlace = HomeController.db.ParkingPlace.FirstOrDefault(c => c.Id == parkingPlace.Id);
            postParkingPlace.Place.UserId = parkingPlace.Place.UserId;
            postParkingPlace.Place.PaidUpTo = DateTime.Now.AddDays(1);
            Models.User user = HomeController.db.User.FirstOrDefault(c => c.Id == postParkingPlace.Place.UserId);
            user.Balance -= postParkingPlace.Place.CostPerDay;
            postParkingPlace.Place.StatusId = 1;
            HomeController.db.SaveChanges();
            Models.Payment payment = new Models.Payment();
            payment.Value = postParkingPlace.Place.CostPerDay;
            payment.PlaceId = postParkingPlace.Place.Id;
            payment.CreatedDate = DateTime.Now;
            HomeController.db.Payment.Add(payment);
            HomeController.db.SaveChanges();

        }
        [Route("api/DeleteRentUserPlace")]
        [HttpDelete]
        public void DeleteRentUserPlace(int Id)
        {
            Models.ParkingPlace postParkingPlace = HomeController.db.ParkingPlace.FirstOrDefault(c => c.Id == Id);
            postParkingPlace.Place.UserId = null;
            postParkingPlace.Place.StatusId = 3;
            HomeController.db.SaveChanges();
        }
        public class ListParkingPlace
        {
            public List<ParkingAppWebAPI.Models.ParkingPlace> listParkingPlace { get; set; }
        }
        public void PostAddUserToPlace(string Id)
        {

        }
        // POST: api/ParkingPlace
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ParkingPlace/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ParkingPlace/5
        public void Delete(int id)
        {
        }
    }
}

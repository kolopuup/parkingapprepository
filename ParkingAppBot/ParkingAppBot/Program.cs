﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ParkingAppBot
{
    class Program
    {
        private const string token = "5160811509:AAHBXxW2mz0LQVlswlUQD22fVBXu7R3yobU";
        private static TelegramBotClient client;
        static void Main(string[] args)
        {
            client = new TelegramBotClient(token);
            client.OnMessage += Client_OnMessage;
            client.StartReceiving();
            Console.ReadLine();
            client.StopReceiving();
        }
        static int question = 0;
        private async static void Client_OnMessage(object sender, MessageEventArgs e)
        {
            await client.SendTextMessageAsync(e.Message.Chat.Id, $"{e.Message.Chat.Id}");
            if(question == 1)
            {
                string[] LoginPassword = e.Message.Text.Split(':');
                bool status = CheckAccount(LoginPassword[0], LoginPassword[1]);
                if (status == false)
                {
                    question = 0;
                }
                else
                {
                    await client.SendTextMessageAsync(e.Message.Chat.Id, "Вы ввели неверный логин или пароль либо аккаунт уже привязан к другому пользователю. Введите данные повторно");

                }
            }
            if (e.Message.Text == "/start")
                await client.SendTextMessageAsync(e.Message.Chat.Id, $"Привет @{e.Message.Chat.Username}, я бот помогающий людям узнавать статус парковочного места. Ты можешь привязать свой Telegram к аккаунту нашего сервиса, для этого введи /link");
            if (e.Message.Text == "/link")
            {
                await client.SendTextMessageAsync(e.Message.Chat.Id, $"Для того что бы привязать аккаунт введите логин и пароль в таком формате:\nlogin:password");
                question = 1;
            }
        }
        private static HttpClient httpClient = new HttpClient();
        private static bool CheckAccount(string login, string password)
        {
            Task<string> c = httpClient.GetAsync($"http://localhost:52706/api/UserIsExisting?login={login}&password={password}").Result.Content.ReadAsStringAsync();
            return Convert.ToBoolean(c.Result);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthorizationPage : ContentPage
    {
        public AuthorizationPage()
        {
            InitializeComponent();
        }

        private async void BSuccess_Clicked(object sender, EventArgs e)
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/UserIsExisting?login={TBLogin.Text}&password={TBPassword.Text}");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            Model.User user = JsonConvert.DeserializeObject<Model.User>(await dataTask);
            if(user is null)
            {
                await DisplayAlert("Error", "User is null", "OK");
                return;
            }
            if(user.RoleId == 1)
            {
                await DisplayAlert("Error", "User is null", "OK");
                return;
            }
            App.loggedUser = user;
            Navigation.PushAsync(new MainMenuPage());

        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactWithUsPage : ContentPage
    {
        public ContactWithUsPage()
        {
            InitializeComponent();
            Refresh();
        }
        private async void Refresh()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetContact");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            Contact contact = JsonConvert.DeserializeObject<Contact>(await dataTask);
            if (contact is null)
            {
                await DisplayAlert("Error", "Contact is null", "OK");
                return;
            }
            TBPhoneNumber.Text = contact.PhoneNumber;
            TBEmail.Text = contact.Email;
        }

        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
        class Contact
        {
            public string PhoneNumber { get; set; }
            public string Email { get; set; }
        }
    }
}
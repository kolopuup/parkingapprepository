﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RentFreelyParkingPlacesPage : ContentPage
    {
        public RentFreelyParkingPlacesPage()
        {
            InitializeComponent();
            Refresh();
        }
        private async void Refresh()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetFreelyPlaces");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            ListParkingPlace listParkingPlace = JsonConvert.DeserializeObject<ListParkingPlace>(await dataTask);
            LVParkingPlace.BindingContext = listParkingPlace;
        }
        class ListParkingPlace
        {
            public List<Model.ParkingPlace> listParkingPlace { get; set; }
        }
        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void BAddParkingPlaceToUser_Clicked(object sender, EventArgs e)
        {
            if(LVParkingPlace.SelectedItem is null)
            {
                await DisplayAlert("Error", "Выберите парковочное место из списка", "OK");
                return;
            }
            Model.ParkingPlace parkingPlace = (Model.ParkingPlace)LVParkingPlace.SelectedItem;
            if(parkingPlace.Place.CostPerDay > App.loggedUser.Balance)
            {
                await DisplayAlert("Error", "Вашего баланса не хватает для оплаты одного дня парковки", "OK");
                return;
            }
            parkingPlace.Place.UserId = App.loggedUser.Id;
            string json = JsonConvert.SerializeObject(parkingPlace);
            HttpContent content = new StringContent(@json, Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await App.client.PostAsync($"{App.ConnectionIP}/api/PostRentUserPlace", content);
            await DisplayAlert("Status", "Rented", "Ok");
            await Navigation.PopAsync();
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddCreditCardPage : ContentPage
    {
        public AddCreditCardPage()
        {
            InitializeComponent();
        }

        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void BAddCard_Clicked(object sender, EventArgs e)
        {
            Model.CreditCard creditCard = new Model.CreditCard();
            creditCard.ActionDate = $"{TBYear.Text}/{TBMonth.Text}";
            creditCard.Number = $"{TBOneNumber.Text}{TBTwoNumber.Text}{TBThreeNumber.Text}{TBFourNumber.Text}";
            creditCard.CVV = TBCVV.Text;
            creditCard.UserId = App.loggedUser.Id;
            string json = JsonConvert.SerializeObject(creditCard);
            HttpContent content = new StringContent(@json, Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await App.client.PostAsync($"{App.ConnectionIP}/api/AddCreditCard", content);
            await DisplayAlert("Success", "Added", "Ok");
        }
    }
}
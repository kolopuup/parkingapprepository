﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuPage : ContentPage
    {
        public MainMenuPage()
        {
            InitializeComponent();
        }

        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void BPayParkingPlace_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PayForParkingPlacePage());
        }

        private void BContactWithUs_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ContactWithUsPage());
        }

        private void BRentParkingPlace_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RentFreelyParkingPlacesPage());
        }

        private void BCheckExpenses_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CheckMyExpensesPage());
        }

        private void BMyParkingPlace_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ClientParkingPlacesPage());
        }
    }
}
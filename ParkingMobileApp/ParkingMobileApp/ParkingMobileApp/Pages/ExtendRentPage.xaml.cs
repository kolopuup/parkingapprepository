﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExtendRentPage : ContentPage
    {
        Model.ParkingPlace postParkingPlace;
        PlaceSum placeSum = new PlaceSum();
        public ExtendRentPage(Model.ParkingPlace parkingPlace)
        {
            InitializeComponent();
            postParkingPlace = parkingPlace;
            this.BindingContext = postParkingPlace;
            DateTime? dt = postParkingPlace.Place.PaidUpTo;
            TBPaidUpTo.Text = $"{dt.Value.Day}.{dt.Value.Month}.{dt.Value.Year}";
            TBBalance.Text = App.loggedUser.Balance.ToString();
        }

        private void Label_BindingContextChanged(object sender, EventArgs e)
        {

        }

        private async void BSuccess_Clicked(object sender, EventArgs e)
        {
            placeSum.Sum = Convert.ToDouble(TBCost.Text);
            if(placeSum.Sum > App.loggedUser.Balance)
            {
                await DisplayAlert("Error", "Вы не можете списать денег больше чем есть на балансе", "OK");
                return;
            }
            placeSum.parkingPlace = postParkingPlace;
            string json = JsonConvert.SerializeObject(placeSum);
            HttpContent content = new StringContent(@json, Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await App.client.PostAsync($"{App.ConnectionIP}/api/ExtendRent", content);
            Task<string> response = httpResponseMessage.Content.ReadAsStringAsync();
            string res = await response;
            await DisplayAlert("Status", $"Ваш баланса: {res}", "Ok");
            App.loggedUser.Balance = Convert.ToDouble(res);
            Navigation.PopAsync();
        }
        class PlaceSum
        {
            public double Sum { get; set; }
            public Model.ParkingPlace parkingPlace { get; set; }
        }
        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void TBAddDay_TextChanged(object sender, TextChangedEventArgs e)
        {
            int result;
            if (int.TryParse(TBAddDay.Text, out result))
            {
                TBCost.Text = Convert.ToString(postParkingPlace.Place.CostPerDay * result);
            }
        }
    }
}
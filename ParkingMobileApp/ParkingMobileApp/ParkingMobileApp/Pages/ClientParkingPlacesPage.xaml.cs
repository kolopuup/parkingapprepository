﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClientParkingPlacesPage : ContentPage
    {
        public ClientParkingPlacesPage()
        {
            InitializeComponent();
            Refresh();
        }
        private async void Refresh()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetUserPlaces?Id={App.loggedUser.Id}");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            ListParkingPlace listParkingPlace = JsonConvert.DeserializeObject<ListParkingPlace>(await dataTask);
            LVParkingPlace.BindingContext = listParkingPlace;
        }
        class ListParkingPlace
        {
            public List<Model.ParkingPlace> listParkingPlace { get; set; }
        }

        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void BRemoveRent_Clicked(object sender, EventArgs e)
        {
            if (LVParkingPlace.SelectedItem is null)
            {
                await DisplayAlert("Error", "Выберите парковочное место из списка", "OK");
                return;
            }
            Model.ParkingPlace parkingPlace = (Model.ParkingPlace)LVParkingPlace.SelectedItem;
            HttpResponseMessage httpResponseMessage = await App.client.DeleteAsync($"{App.ConnectionIP}/api/DeleteRentUserPlace?Id={parkingPlace.Id}");
            await DisplayAlert("Status", "Unrented", "Ok");
            await Navigation.PopAsync();
        }

        private void BExtendRent_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ExtendRentPage((Model.ParkingPlace)LVParkingPlace.SelectedItem));
        }
    }
}
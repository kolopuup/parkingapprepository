﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CheckMyExpensesPage : ContentPage
    {
        public CheckMyExpensesPage()
        {
            InitializeComponent();
            Refresh();
        }

        private async void Refresh()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetPaymentConcreteUser?Id={App.loggedUser.Id}");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            ConceretUserPaymentList listParkingPlace = JsonConvert.DeserializeObject<ConceretUserPaymentList>(await dataTask);
            this.BindingContext = listParkingPlace;
        }
        public class ConceretUserPaymentList
        {
            public List<Model.Payment> payments { get; set; }
        }
        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
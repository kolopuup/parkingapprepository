﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParkingMobileApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PayForParkingPlacePage : ContentPage
    {
        UserSum userSum = new UserSum();
        public PayForParkingPlacePage()
        {
            InitializeComponent();
            this.BindingContext = userSum;
            SetUserCards();
        }
        List<Model.CreditCard> creditCards;
        private async void SetUserCards()
        {
            HttpResponseMessage httpResponseMessage = await App.client.GetAsync($"{App.ConnectionIP}/api/GetAllCardConcreteUser?Id={App.loggedUser.Id}");
            Task<string> dataTask = httpResponseMessage.Content.ReadAsStringAsync();
            creditCards = JsonConvert.DeserializeObject<List<Model.CreditCard>>(await dataTask);
            CBPayCard.ItemsSource = creditCards;
        }

        private void CBPayCard_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

        private void BAddPayCard_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddCreditCardPage());
        }

        private async void BPay_Clicked(object sender, EventArgs e)
        {
            
            userSum.Sum = Convert.ToDouble(TBAddBalance.Text);
            userSum.User = App.loggedUser;
            string json = JsonConvert.SerializeObject(userSum);
            HttpContent content = new StringContent(@json, Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await App.client.PostAsync($"{App.ConnectionIP}/api/AddBalance", content);
            Task<string> response = httpResponseMessage.Content.ReadAsStringAsync();
            string res = await response;
            await DisplayAlert("Status", $"Ваш баланса: {res}", "Ok");
            App.loggedUser.Balance = Convert.ToDouble(res);
        }
        class UserSum
        {
            public double Sum { get; set; }
            public Model.User User { get; set; }
        }
        private void BBack_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    public class Parking
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<int> HorizontalSize { get; set; }
        public Nullable<int> VerticalSize { get; set; }
    }
}

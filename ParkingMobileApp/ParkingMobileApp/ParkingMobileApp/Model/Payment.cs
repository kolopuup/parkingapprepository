﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    public class Payment
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public int PlaceId { get; set; }
        public Place Place { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    public class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public string TelegramChatId { get; set; }
        public double? Balance { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    public class Place
    {
        public int Id { get; set; }
        public Nullable<int> Number { get; set; }
        public Nullable<int> UserId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int StatusId { get; set; }
        public Nullable<double> CostPerDay { get; set; }
        public Nullable<DateTime> PaidUpTo { get; set; }
        public Status Status { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    public class ParkingPlace
    {
        public int Id { get; set; }
        public int ParkingId { get; set; }
        public int PalaceId { get; set; }
        public Parking Parking { get; set; }
        public Place Place { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingMobileApp.Model
{
    class CreditCard
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string ActionDate { get; set; }
        public string CVV { get; set; }
        public int UserId { get; set; }
        [JsonIgnore]
        public string NumberActionDate { get { return $"**** **** **** {Number.Substring(Number.Length - 4)} & {ActionDate}"; } set { } }
    }
}

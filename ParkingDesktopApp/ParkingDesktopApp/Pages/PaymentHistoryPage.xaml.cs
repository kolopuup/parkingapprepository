﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for PaymentHistoryPage.xaml
    /// </summary>
    public partial class PaymentHistoryPage : Page
    {
        public PaymentHistoryPage()
        {
            InitializeComponent();
            DGPayment.ItemsSource = MainWindow.db.Payment.ToList();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}

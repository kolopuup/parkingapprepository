﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for AddEditParkingPage.xaml
    /// </summary>
    public partial class AddEditParkingPage : Page
    {
        Model.Parking postParking;
        public AddEditParkingPage(Model.Parking parking)
        {
            InitializeComponent();
            postParking = parking;
            this.DataContext = postParking;
            TBX.Text = "500";
            TBY.Text = "500";
            if (postParking.Id != 0)
            {
                TBX.Text = postParking.HorizontalSize.ToString();
                TBY.Text = postParking.VerticalSize.ToString();
                Refresh();
            }
        }
        private static List<Rectangle> rectangles = new List<Rectangle>();
        int name = 0;
        private static List<Model.ParkingPlace> parkingPlaces;
        private void Refresh()
        {
            parkingPlaces = MainWindow.db.ParkingPlace.Where(c => c.ParkingId == postParking.Id).ToList();
            foreach (var c in parkingPlaces)
            {
                Rectangle rectangle = new Rectangle();
                rectangle.Name = Convert.ToString("created" + name++);
                rectangle.Width = 35;
                rectangle.Height = 50;
                if (c.Place.Status.Color == "Red")
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Red);
                }
                if (c.Place.Status.Color == "Yellow")
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Yellow);
                }
                if (c.Place.Status.Color == "Green")
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Green);
                }
                Canvas.SetLeft(rectangle, c.Place.X);
                Canvas.SetTop(rectangle, c.Place.Y);
                rectangle.MouseMove += Rectangle_MouseMove;
                rectangles.Add(rectangle);
                rectangle.DataContext = c.Place;
                CParking.Children.Add(rectangle);
            }
        }
        private void BAddPlace_Click(object sender, RoutedEventArgs e)
        {
            Rectangle rectangle = new Rectangle();
            rectangle.Name = Convert.ToString("name"+name++);
            rectangle.Width = 35;
            rectangle.Height = 50;
            rectangle.Fill = new SolidColorBrush(Colors.Green);
            Canvas.SetLeft(rectangle, 50);
            Canvas.SetTop(rectangle, 50);
            rectangle.MouseMove += Rectangle_MouseMove;
            CParking.Children.Add(rectangle);
            rectangles.Add(rectangle);
        }

        private void BDeletPlace_Click(object sender, RoutedEventArgs e)
        {
            if (rectangles.LastOrDefault().Name.Contains("created"))
            {
                Model.ParkingPlace lastParkingPlace = parkingPlaces.LastOrDefault();
                if(!(lastParkingPlace.Place.UserId is null))
                {
                    Model.User user = MainWindow.db.User.FirstOrDefault(c => c.Id == lastParkingPlace.Place.UserId);
                    if (MessageBox.Show($"This place are rented by:\n{user.FullName}\nDelete place?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {

                        MainWindow.db.Place.Remove(MainWindow.db.ParkingPlace.FirstOrDefault(c => c.Id == lastParkingPlace.Id).Place);
                        MainWindow.db.ParkingPlace.Remove(lastParkingPlace);
                        parkingPlaces.Remove(lastParkingPlace);
                        MainWindow.db.SaveChanges();
                    }
                    else
                        return;
                }
            }    
            CParking.Children.Remove(rectangles.LastOrDefault());
            rectangles.Remove(rectangles.LastOrDefault());
        }

        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            string errorMessage = "";
            if (string.IsNullOrWhiteSpace(TBName.Text))
                errorMessage += "Name is null\n";
            if (string.IsNullOrWhiteSpace(TBAddress.Text))
                errorMessage += "Address is null\n";
            if(errorMessage != "")
            {
                MessageBox.Show(errorMessage);
                return;
            }    
            Model.Parking parking = postParking;
            parking.Address = TBAddress.Text;
            parking.Name = TBName.Text;
            parking.HorizontalSize = Convert.ToInt32(TBX.Text);
            parking.VerticalSize = Convert.ToInt32(TBY.Text);
            if(postParking.Id == 0)
            {
                MainWindow.db.Parking.Add(parking);
            }
            MainWindow.db.SaveChanges();
            foreach(var p in rectangles)
            {
                Model.Place place = new Model.Place();
                place.X = Convert.ToInt32(Canvas.GetLeft(p));
                place.Y = Convert.ToInt32(Canvas.GetTop(p));
                place.StatusId = 3;
                place.CostPerDay = 10;
                if (p.Name.Contains("name"))
                {
                    MainWindow.db.Place.Add(place);
                }
                MainWindow.db.SaveChanges();
                Model.ParkingPlace parkingPlace = new Model.ParkingPlace();
                parkingPlace.ParkingId = parking.Id;
                parkingPlace.PalaceId = place.Id;
                if(p.Name.Contains("name"))
                {
                    MainWindow.db.ParkingPlace.Add(parkingPlace);
                }
                MainWindow.db.SaveChanges();
            }
            MainWindow.db.SaveChanges();
                MessageBox.Show("Saved");
            rectangles = new List<Rectangle>();
            parkingPlaces = new List<Model.ParkingPlace>();
            NavigationService.Navigate(new ParkingPage());
        }

        private void TBX_TextChanged(object sender, TextChangedEventArgs e)
        {
            int result;
            if (int.TryParse(TBX.Text, out result))
                CParking.Width = result;
            else
                TBX.Text = "500";
        }

        private void TBY_TextChanged(object sender, TextChangedEventArgs e)
        {
            int result;
            if (int.TryParse(TBY.Text, out result))
                CParking.Height = result;
            else
                TBY.Text = "500";
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            rectangles = new List<Rectangle>();
            parkingPlaces = new List<Model.ParkingPlace>();
            NavigationService.Navigate(new ParkingPage());
        }
        private void CParking_DragOver(object sender, DragEventArgs e)
        {
            Point dropCoor = e.GetPosition(CParking);

            Canvas.SetLeft(rectangles[selectedRectangle], dropCoor.X);
            Canvas.SetTop(rectangles[selectedRectangle], dropCoor.Y);
            if(postParking.Id != 0)
            {
                ((Model.Place)rectangles[selectedRectangle].DataContext).X = Convert.ToInt32(Canvas.GetLeft(rectangles[selectedRectangle]));
                ((Model.Place)rectangles[selectedRectangle].DataContext).Y = Convert.ToInt32(Canvas.GetTop(rectangles[selectedRectangle]));
            }
        }
        private static int selectedRectangle;
        private void Rectangle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            { 
                selectedRectangle = rectangles.FindIndex(c => c.Name == ((Rectangle)sender).Name);
                DragDrop.DoDragDrop((Rectangle)sender, (Rectangle)sender, DragDropEffects.Move);
            }
        }
    }
}

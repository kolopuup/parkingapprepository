﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for ParkingPage.xaml
    /// </summary>
    public partial class ParkingPage : Page
    {
        public ParkingPage()
        {
            InitializeComponent();
            Refresh();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MainMenuPage());
        }

        private void LVParking_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LVParking.SelectedItem is null)
                return;
            NavigationService.Navigate(new Pages.DetailsParkingPage((Model.Parking)LVParking.SelectedItem));
        }

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AddEditParkingPage(new Model.Parking()));
        }

        private void BEdit_Click(object sender, RoutedEventArgs e)
        {
            if (LVParking.SelectedItem is null)
            {
                MessageBox.Show("Select Item");
                return;
            }
            NavigationService.Navigate(new AddEditParkingPage((Model.Parking)LVParking.SelectedItem));
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (LVParking.SelectedItem is null)
            {
                MessageBox.Show("Select Item");
                return;
            }
            if (MessageBox.Show("Delete Parking?", "Success", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MainWindow.db.Parking.Remove((Model.Parking)LVParking.SelectedItem);
                MainWindow.db.SaveChanges();
                MessageBox.Show("Delete success");
                Refresh();
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) => Refresh();
        private void Refresh()
        {
            MainWindow.db = new Model.ParkingAppDatabaseEntities();
            LVParking.ItemsSource = null;
            LVParking.ItemsSource = MainWindow.db.Parking.ToList();
        }

        private void BSetStaffToPlace_Click(object sender, RoutedEventArgs e)
        {
            if (!Validation()) return;
            Windows.SetStaffToParkingWindow setStaffToParkingWindow = new Windows.SetStaffToParkingWindow((Model.Parking)LVParking.SelectedItem);
            setStaffToParkingWindow.ShowDialog();
            Refresh();
        }
        private bool Validation()
        {
            if(LVParking.SelectedItem is null)
            {
                MessageBox.Show("Выберите парковку");
                return false;
            }
            return true;
        }
        private void BDeteteStaffFromPlace_Click(object sender, RoutedEventArgs e)
        {
            if (!Validation()) return;
            Windows.DeleteStaffFromPlaceWindow setStaffToParkingWindow = new Windows.DeleteStaffFromPlaceWindow((Model.Parking)LVParking.SelectedItem);
            setStaffToParkingWindow.ShowDialog();
            Refresh();
        }
    }
}

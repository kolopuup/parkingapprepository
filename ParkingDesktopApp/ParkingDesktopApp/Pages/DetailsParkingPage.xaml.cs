﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ParkingDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for DetailsParkingPage.xaml
    /// </summary>
    public partial class DetailsParkingPage : Page
    {
        Model.Parking postParking;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        public DetailsParkingPage(Model.Parking parking)
        {
            InitializeComponent();
            postParking = parking;
            TBName.Text = parking.Name;
            CParkingPlace.Height = Convert.ToDouble(parking.VerticalSize);
            CParkingPlace.Width = Convert.ToDouble(parking.HorizontalSize);
            Refresh();
            dispatcherTimer.Interval = TimeSpan.FromSeconds(1);
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Start();

        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            CParkingPlace.Children.Clear();
            Refresh();
        }

        private void Refresh()
        {
            Model.ParkingAppDatabaseEntities db = new Model.ParkingAppDatabaseEntities();
            List<Model.Place> parkingPlaces = db.ParkingPlace.Where(c => c.ParkingId == postParking.Id).Select(b=>b.Place).ToList();
            foreach(var c in parkingPlaces)
            {
                Rectangle rectangle = new Rectangle();
                rectangle.Width = 35;
                rectangle.Height = 50;
                Canvas.SetLeft(rectangle, c.X);
                Canvas.SetTop(rectangle, c.Y);
                if(c.Status.Color == "Red")
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Red);
                }
                if (c.Status.Color == "Yellow")
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Yellow);
                }
                if (c.Status.Color == "Green")
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Green);
                }

                Grid grid = new Grid();
                grid.Children.Add(rectangle);
                TextBlock textBlock = new TextBlock();

                textBlock.FontSize = 25;
                textBlock.Text = c.Number.ToString();
                textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                textBlock.VerticalAlignment = VerticalAlignment.Center;
                grid.Children.Add(textBlock);

                grid.MouseLeftButtonDown += Rectangle_MouseLeftButtonDown;
                Canvas.SetLeft(grid, c.X);
                Canvas.SetTop(grid, c.Y);

                grid.DataContext = c;
                CParkingPlace.Children.Add(grid);
            }
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Model.Place place = (Model.Place)(((Grid)sender).DataContext);
            Model.ParkingPlace parkingPlace = MainWindow.db.ParkingPlace.FirstOrDefault(c => c.PalaceId == place.Id);
            Windows.ParkingPlaceDetailsWindow parkingPlaceDetailsWindow = new Windows.ParkingPlaceDetailsWindow(parkingPlace);
            parkingPlaceDetailsWindow.ShowDialog();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}

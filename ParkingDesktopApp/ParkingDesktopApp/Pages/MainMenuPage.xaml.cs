﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for MainMenuPage.xaml
    /// </summary>
    public partial class MainMenuPage : Page
    {
        public MainMenuPage()
        {
            InitializeComponent();
        }

        private void BAllParking_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ParkingPage());
        }

        private void BAllEmployee_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new EmployeePage());
        }

        private void BAllPayment_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PaymentHistoryPage());
        }

        private void BAllClient_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ClientPage());
        }

        private void BChangeContact_Click(object sender, RoutedEventArgs e)
        {
            Windows.ChangeContactWindow changeContactWindow = new Windows.ChangeContactWindow();
            changeContactWindow.ShowDialog();
        }
    }
}

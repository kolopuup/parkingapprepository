﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Pages
{
    /// <summary>
    /// Interaction logic for ClientPage.xaml
    /// </summary>
    public partial class ClientPage : Page
    {
        public ClientPage()
        {
            InitializeComponent();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private bool Validation()
        {
            if(DGEmployee.SelectedItem is null)
            {
                MessageBox.Show("Выберите клиента");
                return false;
            }
            return true;
        }
        private void Refresh()
        {
            DGEmployee.ItemsSource = null;
            DGEmployee.ItemsSource = MainWindow.db.User.Where(c => c.RoleId == 2).ToList();
        }
        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            Windows.AddEditEmployeeClientWindow addEditEmployeeClientWindow = new Windows.AddEditEmployeeClientWindow(new Model.User());
            addEditEmployeeClientWindow.ShowDialog();
            Refresh();
        }
        private void BEdit_Click(object sender, RoutedEventArgs e)
        {
            if (!Validation()) return;
            Windows.AddEditEmployeeClientWindow addEditEmployeeClientWindow = new Windows.AddEditEmployeeClientWindow((Model.User)DGEmployee.SelectedItem);
            addEditEmployeeClientWindow.ShowDialog();
            Refresh();
        }

        private void BDelte_Click(object sender, RoutedEventArgs e)
        {
            if (!Validation()) return;
            MainWindow.db.User.Remove((Model.User)DGEmployee.SelectedItem);
            MainWindow.db.SaveChanges();
            Refresh();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}

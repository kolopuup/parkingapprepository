﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for AddEditEmployeeClientWindow.xaml
    /// </summary>
    public partial class AddEditEmployeeClientWindow : Window
    {
        Model.User postUser;
        public AddEditEmployeeClientWindow(Model.User user)
        {
            InitializeComponent();
            postUser = user;
            this.DataContext = postUser;
            if (postUser.Role is null)
            {
                CBRole.SelectedIndex = 0;
            }
        }

        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            string errorMessage = "";
            if (string.IsNullOrWhiteSpace(TBFullName.Text))
            {
                errorMessage += "\nЗаполните поле с именем";
            }
            if (string.IsNullOrWhiteSpace(TBLogin.Text))
            {
                errorMessage += "\nЗаполните логин";
            }
            if(string.IsNullOrWhiteSpace(TBPassword.Text))
            {
                errorMessage += "\nЗаполните пароль";
            }
            if(errorMessage != "")
            {
                MessageBox.Show(errorMessage);
                return;
            }
            if(postUser.Id == 0)
            {
                MainWindow.db.User.Add(postUser);
            }
            MainWindow.db.SaveChanges();
            MessageBox.Show("Пользователь добавлен");
            this.Close();
        }
    }
}

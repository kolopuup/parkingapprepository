﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for ChangeContactWindow.xaml
    /// </summary>
    public partial class ChangeContactWindow : Window
    {
        public ChangeContactWindow()
        {
            InitializeComponent();

            this.DataContext = MainWindow.db.Contact.FirstOrDefault();
        }

        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainWindow.db.SaveChanges();

                MessageBox.Show("Сохранено");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Ошибка, введите корректные данные");
                return;
            }
        }
    }
}

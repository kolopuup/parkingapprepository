﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for SetStaffToParkingWindow.xaml
    /// </summary>
    public partial class SetStaffToParkingWindow : Window
    {
        Model.Parking postParking;
        public SetStaffToParkingWindow(Model.Parking parking)
        {
            InitializeComponent();
            postParking = parking;
            List<Model.ParkingSecurity> parkingSecurities = MainWindow.db.ParkingSecurity.Where(c => c.ParkingId == parking.Id).ToList();
            List<Model.User> users = MainWindow.db.User.Where(c => c.RoleId == 1 && c.ParkingSecurity.Count == 0).ToList();
            foreach(var c in parkingSecurities)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    if(c.UserId == users[i].Id)
                    {
                        users.Remove(users[i]);
                    }
                }
            }
            DGEmployee.ItemsSource = users;
        }

        private void DGEmployee_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Model.ParkingSecurity parking = new Model.ParkingSecurity();
            parking.UserId = ((Model.User)DGEmployee.SelectedItem).Id;
            parking.ParkingId = postParking.Id;
            MainWindow.db.ParkingSecurity.Add(parking);
            MainWindow.db.SaveChanges();
            MessageBox.Show("Охраник назначен");
            this.Close();
        }
    }
}

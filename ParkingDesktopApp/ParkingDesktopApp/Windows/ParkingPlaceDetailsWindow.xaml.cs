﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for ParkingPlaceDetailsWindow.xaml
    /// </summary>
    public partial class ParkingPlaceDetailsWindow : Window
    {
        Model.ParkingPlace postParkingPlace;
        public ParkingPlaceDetailsWindow(Model.ParkingPlace parkingPlace)
        {
            InitializeComponent();
            postParkingPlace = parkingPlace;
            this.DataContext = postParkingPlace;
        }

        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            int result;
            if(int.TryParse(TBNumber.Text, out result))
            {
                if(MainWindow.db.ParkingPlace.Where(c=>c.ParkingId == postParkingPlace.ParkingId).Select(c=>c.Place).FirstOrDefault(c=>c.Number == result) != null)
                {
                    MessageBox.Show("Такое номер уже существует");
                    return;
                }
                postParkingPlace.Place.Number = result;
                MainWindow.db.SaveChanges();
                this.Close();
            }
            else
            {
                MessageBox.Show("Введите число");
                return;
            }
        }
    }
}

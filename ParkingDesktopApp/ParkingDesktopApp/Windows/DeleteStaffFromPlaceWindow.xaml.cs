﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParkingDesktopApp.Windows
{
    /// <summary>
    /// Interaction logic for DeleteStaffFromPlaceWindow.xaml
    /// </summary>
    public partial class DeleteStaffFromPlaceWindow : Window
    {
        Model.Parking postParking;
        public DeleteStaffFromPlaceWindow(Model.Parking parking)
        {
            InitializeComponent();
            postParking = parking;
            DGEmployee.ItemsSource = MainWindow.db.ParkingSecurity.Where(c => c.ParkingId == parking.Id).ToList();
        }

        private void DGEmployee_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MainWindow.db.ParkingSecurity.Remove((Model.ParkingSecurity)DGEmployee.SelectedItem);
            MainWindow.db.SaveChanges();
            MessageBox.Show("Охраник отстранён");
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingDesktopApp.Model
{
    partial class Parking
    {
        public int FreelyPlaceCount
        {
            get
            {
                return this.ParkingPlace.Where(c=>c.Place.Status.Name == "Freely").Count();
            }
            set
            {

            }
        }
        public int OccupiedPlaceCount
        {
            get
            {
                return this.ParkingPlace.Where(c => c.Place.Status.Name == "Occupied").Count();
            }
            set
            {

            }
        }
    }
}
